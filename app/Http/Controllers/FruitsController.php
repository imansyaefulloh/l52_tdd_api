<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fruit;
use App\Http\Requests;
use App\Controllers\BaseController;
use App\Transformers\FruitsTransformer;
use App\Http\Requests\StoreFruitRequest;

class FruitsController extends Controller
{
    public function index()
	{
	    $fruits = Fruit::all();

	    // return $this->response->array(['data' => $fruits], 200);
	    return $this->collection($fruits, new FruitsTransformer);
	}

	public function show($id)
	{
	    $fruit = Fruit::where('id', $id)->first();

	    if ($fruit) {
	        return $this->item($fruit, new FruitsTransformer);
	    }

	    return $this->response->errorNotFound();
	}

	public function store(StoreFruitRequest $request)
	{
	    if (Fruit::create($request->all())) {
	        return $this->response->created();
	    }

	    return $this->response->errorBadRequest();
	}

	/**
	 * Remove the specified fruit.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	    $fruit = Fruit::find($id);

	    if ($fruit) {
	        $fruit->delete();
	        return $this->response->noContent();
	    }

	    return $this->response->errorBadRequest();
	}
}
