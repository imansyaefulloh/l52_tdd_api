<?php

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        DB::table('users')->delete();

        $users = [
            [
                'name' => 'iman',
                'email' => 'iman@gmail.com',
                'password' => bcrypt('secret')
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }

        Model::reguard();
    }
}
